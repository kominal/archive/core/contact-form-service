import { getEnvironmentString } from '@kominal/lib-node-environment';

export const RECEIVER_ADDRESS = getEnvironmentString('RECEIVER_ADDRESS', 'mail@kominal.com');
